#!/bin/bash
# This script will run any job in here on the jobs server nightly.
# /locm/spark_jobs/bin/submit.sh $CLASS: Required $MASTER: Required $SPARK_ARGS: Optional $CLASS_ARGS: Optional
#

#Add Sweeper stuff here

#Add UpdateGraphiteBatch stuff here

#Stuff Related to IpCollusionDetector
/locm/spark_jobs/bin/submit.sh IpCollusionDetector yarn-client
rm -f /locm/spark_jobs/IPCollusion_*.xlsx

#Stuff related to UniqueUsers Job
MONTH=`date +"%m"`
YEAR=`date +"%Y"`
/locm/spark_jobs/bin/submit.sh UniqueUsers yarn-client "" "-month $MONTH -year $YEAR"