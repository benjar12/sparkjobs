import com.mongodb.casbah.Imports._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.json4s.JsonAST.JValue
import service.{GraphiteWriter, GraphiteMongoWriter, GraphiteConfigService, AdConfigService}

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._
import service.AdConfigService.Publisher
import util.{TimeZoneFactory, Settings, Logging}

import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
 * Created by benjarman on 5/6/15.
 *
 * Sample config :
 * {
    "collectionName": "PublisherCount",
    "groupBy": [
      ["params",  "prd"],
      ["extparams", "ypaAdConfig", "#publisher"],
      ["extparams", "ypaAdConfig"],
      ["params", "dr"]
    ],
    "filterBy" : [{"index":0, "value": "12"}],
    "nameSpace": ["test","${0}", "publisher_group", "${1}", "${2}", "${3}"]
    "batch": true,
    "streaming": true,
    "topics": ["s"]

  }
 */
object UpdateGraphiteBatch {

  //get our spark context
  val sparkConf = new SparkConf().setAppName("UpdateGraphiteBatch.scala")
  val sc = new SparkContext(sparkConf)

  //magic nums
  val divideBy = 600000
  val divideByToGetSeconds = 1000
  val minusSeconds = 1

  //broadcast the adConfigs to all the workers
  val adConfigs = sc.broadcast(AdConfigService().getMap())

  /*
  Used before the reduceByKey gets the time stamp
  down to hours
   */
  def tsToHours(ts: Long): Long = {
    ts / divideBy
  }

  /*
  Used before writing to graphite and mongo
   */
  def backToTs(hr: Long): Long = {
    ((hr * divideBy) / divideByToGetSeconds) - minusSeconds
  }

  /*
  Used to get publisher from ad config
   */
  def getPublisher(adConfig: String): String = {
    adConfigs.value.getOrElse(adConfig, new Publisher()).publisherName.getOrElse("null")
  }

  /*
  Used by any groupBy value that has a # as the first
  char.
  We are using the # as a means of calling a function.
  For now publisher is the only one were looking up.
   */
  def runFunction(functionName: String, params: Any): String = {
    if (functionName == "#publisher") {
      getPublisher(params.asInstanceOf[String])
    }
    else {
      "null"
    }
  }

  /*
  takes a json value and a list of keys to extract
  the value. If any of the keys contain # at char
  0 we call run function on the last value.
  Params :
    keys : what we travers EX. ["headers", "X-LOCM-IP"]
    jvalue : The parsed json
    lastValue : the last value extracted
   */
  def recursivelyExtractValue(keys: List[String], jvalue: JValue, lastValue: String = ""): String = {

    val key = keys.head
    val current = jvalue \ key

    val value = if (key.head.equals('#')) {
      runFunction(key, lastValue.asInstanceOf[Any])
    } else {
      current.values.toString
    }

    if (keys.length > 1) {
      recursivelyExtractValue(keys.tail, current, value)
    } else {
      if(value.length > 1){value.replace(".", "_")}
      else{"None"}
    }
  }

  /*
  Params :
    values : List[String] EX. ['12', 'Search_Initiatives', '00000015a', '2']
    nameSpace : List[Any] EX. ['orwell', 1, 'publisher_group', 2, 3, 4]

  Returns : "orwell.12.publisher_group.Search_Initiatives.00000015a.2"
   */
  def recursivelyBuildGraphiteNameSpace(values: List[String], nameSpace: List[Any]): String ={
    val next = nameSpace.head match {
      case s: String => s
      case i: Int => values(i)
    }
    if(nameSpace.size > 1){
      next + "." + recursivelyBuildGraphiteNameSpace(values, nameSpace.tail)
    }else{
      next
    }
  }

  /*
  Entry point to our application.

  Steps :
    1. Fetch configurations from mongo
    2. ForEach config
      B. Build rdd path
      A. Map Job
        a. ForEach groupBy key recursively parse value from the json
        b. If key contains # call runFunction with the last recursive
        value as params
      B. If filter.size > 0
        a. foreach filter based on list index and value
      C. ReduceByKey
      D. Collect.foreach
        a. write to mongo
        b. write to graphite
      E. Update config
   */
  def main(args: Array[String]): Unit = {

    val logger = new Logging("UpdateGraphiteBatch.scala")

    val configs = GraphiteConfigService()
    val hdfsSettings = new Settings().getHdfsConf()
    val appSettings = new Settings().getGraphiteConf()
    val hdfsBasePath = hdfsSettings.hdfsHost + hdfsSettings.hdfsBasePath
    val dateFormat = hdfsSettings.dateFormat
    val dailyPath = hdfsSettings.dailyPath

    configs.foreach(config => {
      //get topics
      val topics = config.batchTopics
      //Get the number of days that still need to be processed
      //if no date in record we just process yesterday.
      val endDate = new DateTime(TimeZoneFactory("UTC")).minusDays(1)
      val startDate = config.runFrom.getOrElse(endDate)
      val hdfsPath = f"$hdfsBasePath/${topics}/${dailyPath}/*/"

      /*
      Pre extract the int values from nameSpace so we don't
      Do it on every record. We will then do case matching
      when we use it.
       */
      val nameSpace = config.nameSpace.foldLeft(List[Any]())( (list, next)=>{
        list ++ List( if(next.charAt(0).equals('$')){
          next.dropRight(1).drop(2).toInt
        }else{next} )
      } )

      /*
      Broadcast things to all of the actors. We don't have
      to broadcast namespace because it's used after collect.
       */
      val groupBy = sc.broadcast(config.groupBy)
      val filterBy = sc.broadcast(config.filterBy)

      /*
      use recursion to process dates until
      we reach the end date.
      We expect this to fail if the text file
      does not exist in hdfs so we will try catch
      each file we run.
       */
      def evalDate(current: DateTime): Unit = {

        //extract date string
        val file = hdfsPath + DateTimeFormat.forPattern(hdfsSettings.dateFormat).print(current) + hdfsSettings.ext

        //execute
        try {
          exec(file)
        } catch {
          case e: Exception => logger.error(e.getMessage)
        }

        //process next
        if (endDate.isAfter(current)) {
          evalDate(current.plusDays(1))
        }
      }

      /*
      Where we will do the actual mapping,
      filtering and reducing.
       */
      def exec(filePath: String): Unit = {

        logger.info(s"Starting processing on: $filePath")

        val textFile = sc.textFile(filePath)

        val parsed = textFile.map(r => {
          parse(r)
        })

        /*
        The Tuple Produced here is as follows
        ( ( List[String], ts: Long), 1)
        We may have to change that to
        List[Any]
         */
        val mapped = parsed.map(r => {
          implicit val format = DefaultFormats
          val tsOg = (r \ "ts").values.asInstanceOf[BigInt].toLong
          val ts = if (tsOg != 0L) {
            tsToHours(tsOg)
          } else {
            tsOg
          }
          def gbs = groupBy.value

          val tp = (
            (
              gbs.foldLeft(List[String]())((list, next) => {
                list ++ List(recursivelyExtractValue(next, r))
              }),
              ts
              ),
            1)

          tp
        })

        /*
        Filter anything that has 0L ts or doesn't
        match what is in mongo
         */
        val filtered = mapped.filter(r => {
          val fl = filterBy.value
          r._1._2 != 0L && fl.foldLeft(true)((shouldBeIn, next) => {

            val index = next._1
            val eq = next._2
            val to = r._1._1(index)

            if (eq != to) {
              false
            }
            else {
              shouldBeIn
            }
          })
        })

        /*
        Get counts of everything that matches
        r._1
         */
        val reduced = filtered.reduceByKey((a, b) => a + b)

        /*
        Convert timestamps back from the 10
        minute grouping and flatten the
        tuple
         */
        val reMapped = reduced.map(r => {
          (r._1._1, backToTs( r._1._2 ), r._2)
        })

        /*
        Split the rdd for processing
         */
        val split = reMapped.randomSplit(Array.fill[Double](appSettings.numOfSplits)(1.toDouble / appSettings.numOfSplits))

        /*
        Collect
        1. Build NameSpace
        2. write to mongo
        3. write to graphite
         */
        split.foreach(rdd=>{

          //Create our mongo Writer and Socket Connection to graphite
          val mongoWriter = new GraphiteMongoWriter(appSettings.batchCollection)
          val graphiteSocket = new GraphiteWriter()

          rdd.collect().foreach(r =>{

            //Create our namespace and extract
            //other things we need.
            val ts    = r._2
            val count = r._3
            val ns    = recursivelyBuildGraphiteNameSpace(r._1, nameSpace) + s" %s $ts"
            val formattedNs = ns.format(count.toString)

            logger.info(s"Writting the following to graphite: $formattedNs")

            //write to mongo
            mongoWriter.write(ns, count)

            //write to graphite
            graphiteSocket.write(formattedNs)

          })

          //clean up
          mongoWriter.close()
          graphiteSocket.close()
        })

      }//END OF EXEC

      evalDate(startDate)

      /*
      Update the config with today's date for the runFrom
       */
      GraphiteConfigService.updateNextDayToRun(config)


    })//END OF FOREACH CONFIG


  }//END OF MAIN

}//END OF OBJECT
