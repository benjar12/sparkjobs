import java.net.InetAddress
import scala.annotation.migration
import scala.io.Source
import org.apache.commons.mail.EmailAttachment
import org.apache.commons.mail.MultiPartEmail
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.Interval
import org.joda.time.format.PeriodFormatterBuilder
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jvalue2monadic
import org.json4s.string2JsonInput
import com.norbitltd.spoiwo.model.CellStyle
import com.norbitltd.spoiwo.model.Color
import com.norbitltd.spoiwo.model.Column
import com.norbitltd.spoiwo.model.Font
import com.norbitltd.spoiwo.model.Row
import com.norbitltd.spoiwo.model.Sheet
import com.norbitltd.spoiwo.model.Workbook
import com.norbitltd.spoiwo.model.enums.CellFill
import com.norbitltd.spoiwo.natures.xlsx.Model2XlsxConversions.XlsxWorkbook
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import org.apache.spark.storage.StorageLevel
import com.norbitltd.spoiwo.model.Width
import com.norbitltd.spoiwo.model.enums.CellHorizontalAlignment
import scala.collection.mutable.LinkedHashMap
import scala.collection.immutable.ListMap
import java.util.regex.Pattern

object IpCollusionDetector {

  /**
   * Loads Resource File from classpath. It should be a mapping file composed of "<publisher>,<domain>" lines.
   * This method transforms the file into a lookup map where the key is the domain and the value is the publisher.
   */
  def loadMap(path: String) = {
    Source.fromInputStream(getClass.getResourceAsStream(path)).getLines.map(_.split(",")).map(item => (item(1), item(0))).toMap
  }

  /**
   * Generate the report interval given the reference date and the desired duration.
   * The resulting interval will contain the start date that is x days from the reference date
   * and end date that is right before the reference date.
   */
  def genReportInterval(referenceDate: DateTime, durationInDays: Int) = {
    val startDate = referenceDate.minusDays(durationInDays)
    val finalDate = referenceDate.minusDays(1)
    new Interval(startDate, finalDate)
  }

  /**
   * Generate a file url for hadoop based on the desired date interval
   */
  def genFileUrl(topic: String, interval: Interval) = {
    val fileNames = (for (x <- 0 to interval.toPeriod().getDays()) yield interval.getStart().plusDays(x).toString("yyyy-MM-dd'.bz2'")).mkString(",")
    f"/user/root/orwell/data/$topic/daily/2015/{$fileNames}"
  }

  /**
   * returns a string representation of the interval in "yyyy-MM-dd to yyyy-MM-dd" format
   */
  def str(interval: Interval) = {
    s"${interval.getStart().toString("yyyy-MM-dd")} to ${interval.getEnd().toString("yyyy-MM-dd")}"
  }

  /**
   * returns a string representation of the interval's period in hours.
   */
  def hours(interval: Interval) = {
    // our interval is inclusive range so we need to add another day's worth
    interval.toDuration().getStandardHours() + 24
  }

  /**
   * returns a string representation of the elapsed time between start and end time in the format of hh:mm:ss.
   */
  def elapsed(startTime: DateTime, finalTime: DateTime) = {
    val duration = new Duration(startTime, finalTime)
    val formatter = new PeriodFormatterBuilder()
      .printZeroAlways()
      .minimumPrintedDigits(2) // gives the '01'
      .appendHours()
      .appendSeparator(":")
      .appendMinutes()
      .appendSeparator(":")
      .appendSeconds()
      .toFormatter();
    formatter.print(duration.toPeriod())
  }

  def cellStyle() = {
    CellStyle(fillPattern = CellFill.Solid, fillForegroundColor = Color.LightGrey, fillBackgroundColor = Color.LightGrey, font = Font(bold = true, color = Color.White), horizontalAlignment = CellHorizontalAlignment.Center)
  }

  def autosizedCol(count: Int) = {
    (for (x <- (1 to count)) yield Column(autoSized = true))
  }

  def countList(itemCount: Map[String, Int]) = {
    ListMap(itemCount.toSeq.sortWith(_._2 > _._2): _*).map(i => s"[${i._2}]:${i._1}").mkString(", ")
  }

  def limit(counts: Map[String, Int], threshold: Int): Boolean = {
    counts.foreach { t =>
      if (t._2 >= threshold) {
        return true
      }
    }
    return false
  }

  def genSummarySheet(threshold: Int, summaryStats: Seq[Any], ipList: Seq[(String, Map[String, Int])]) = {
    val summaryRowz = summaryStats.map { r =>
      r match {
        case (desc: String, value: String) => Row().withCellValues(desc, value)
        case (desc: String, value: Int) => Row().withCellValues(desc, value)
        case (desc: String, value: Long) => Row().withCellValues(desc, value)
        case desc: String => Row().withCellValues(desc)
        case None => Row().withCellValues("")
      }
    }

    val summaryRows = scala.collection.mutable.MutableList[Row]()

    var count = 0
    var tempThreshold = threshold
    do {
      val newCount = ipList.filter(x => limit(x._2, tempThreshold)).size
      if (count != newCount) {
        count = newCount
        if (count > 0) {
          summaryRows += Row().withCellValues(f"Total IPs with $tempThreshold%02d distinct referrals within publisher's porfolio", count)
        }
      }
      tempThreshold += 1
    } while (count > 0)

    Sheet(name = "Summary")
      .withColumns(autosizedCol(2): _*)
      .addRow(Row().withCellValues(""))
      .addRows(summaryRowz)
      .addRow(Row().withCellValues(""))
      .addRows(summaryRows)
  }

  def genIpSheet(ipMap: Map[String, Map[String, Int]], scoreCount: Map[String, Long], impressionCount: Map[String, Long], clickCount: Map[String, Long], ipUa: Map[String, Map[String, Int]], ipCookie: Map[String, Map[String, Int]]) = {
    Sheet(name = "Flagged IP Stats",
      Row(style = cellStyle()).withCellValues("IP", "Publishers", "Scores", "Impressions", "Clicks", "CTR", "Cookie", "User Agent")).withColumns(autosizedCol(8): _*)
      .addRows(for ((ip, countMap) <- ipMap.toSeq.sortBy(_._1)) yield {
        val scores = scoreCount.getOrElse(ip, 0L)
        val impressions = impressionCount.getOrElse(ip, 0L)
        val clicks = clickCount.getOrElse(ip, 0L)
        val cookies = ipCookie.get(ip)
        val cookieCount = cookies.map(_.keys.size).getOrElse(0)
        val ua = ipUa.get(ip).map(x => ListMap(x.toSeq.sortWith(_._2 > _._2): _*)).map(x => x.toSeq.apply(0)).map(x => f"[${x._2}]:${x._1}").getOrElse("N/A")

        Row().withCellValues(ip, countList(countMap), scores, impressions, clicks, ctr(impressions, clicks), cookieCount, ua)
      })
  }

  def longIp(ip: String): Long = {
    val octets = ip.split(Pattern.quote("."))
    octets(0).toLong << 24 | octets(1).toLong << 16 | octets(2).toLong << 8 | octets(3).toLong
  }

  def genWorkSheet(keyName: String, itemName: String, keyedItems: Seq[(String, Map[String, Int])]) = {
    Sheet(name = s"${keyName} ${itemName}s",
      Row(style = cellStyle()).withCellValues(keyName, s"Distinct ${itemName}s", s"${itemName} List")).withColumns(autosizedCol(3): _*)
      .addRows(for ((ip, count) <- keyedItems.sortBy(_._1)) yield Row().withCellValues(ip, count.size, countList(count)))
  }

  def ctr(impressions: Long, clicks: Long) = {
    if (impressions > 0)
      f"${(clicks.toDouble / impressions.toDouble) * 100}%2.1f"
    else
      "N/A"
  }

  /**
   * generate and send the email using the provided email config along with subject, body and attachment
   */
  def sendEmailWithAttachement(emailConfig: Config, subject: String, body: String, path: String) = {
    val attachment = new EmailAttachment()
    attachment.setPath(path)
    attachment.setName(path)
    attachment.setDescription(path)
    attachment.setDisposition(EmailAttachment.ATTACHMENT)

    val email = new MultiPartEmail()
    email.setHostName(emailConfig.getString("host"))
    email.setFrom(emailConfig.getString("from.addr"), emailConfig.getString("from.name"))
    email.addTo(emailConfig.getString("to.addr"), emailConfig.getString("to.name"))
    if (emailConfig.hasPath("cc.addr")) {
      email.addCc(emailConfig.getString("cc.addr"), emailConfig.getString("cc.name"))
    }
    email.setSubject(subject)
    email.setMsg(body)
    email.attach(attachment)

    email.send()
  }

  def info(since: DateTime, text: String) = {
    println(f"[${elapsed(since, new DateTime())}] ${text}")
  }

  /**
   * Main App to execute in Spark Job's master node
   */
  def main(args: Array[String]) {

    val config = ConfigFactory.load("IpCollusion.conf")
    val pm = loadMap("publisher.conf")

    val threshold = 9

    val jobStart = new DateTime()
    val reportInterval = genReportInterval(jobStart, config.getInt("job.durationDays"))

    info(jobStart, genFileUrl("s", reportInterval))

    val sparkConf = new SparkConf().setAppName("IpCollusion")
    val sc = new SparkContext(sparkConf)

    // ==============================================================================================
    // 1st part of the job is to flag suspicious IPs based on various threshold counts 
    // ==============================================================================================

    val broadcastedPm = sc.broadcast(pm)
    val totalScoreEvents = sc.accumulator(0L)
    val totalDistinctScoringIp = sc.accumulator(0L)

    // RDD[(ip: String, domain: String)]
    val events = sc.textFile(genFileUrl("s", reportInterval)).map { str =>
      totalScoreEvents += 1
      val json = parse(str)
      val ip = (json \ "headers" \ "X-LOCM-CIP").values.toString()
      val referer = (json \ "headers" \ "Referer").values.toString().drop(7)
      val host = referer.take(referer.indexOf("/")).toLowerCase()
      val domain = if (host.startsWith("www.")) host.drop(4) else host
      val normalized = if (domain.indexOf(".") > 0) domain else (json \ "headers" \ "Referer").values.toString()
      (ip, normalized)
    }.distinct()

    // Pre-filter using the final threshold to trim the initial data set
    // RDD[(ip: String, List[domain: String])]
    val prolificVisitors = events.groupByKey().filter { v =>
      totalDistinctScoringIp += 1
      v._2.size >= threshold
    }

    // RDD[(ip: String, List[publisher: String)]
    val suspiciousVisitors = prolificVisitors.flatMap(v => v._2.map(domain => (v._1, broadcastedPm.value.getOrElse(domain, domain)))).groupByKey()

    // Filter by publisher property threshold
    // Array[(ip: String, Map[publisher: String, count: Int])]
    val flaggedVisitors = suspiciousVisitors.map(v => (v._1, v._2.groupBy(p => p).mapValues(_.size).map(identity))).filter(x => limit(x._2, threshold)).collect()

    // ==============================================================================================
    // Setup for the rest of the job by broadcasting the unique list of IPs that are flagged suspicious
    // ==============================================================================================

    // Collect the filtered IP Set
    val ipSet = flaggedVisitors.map(_._1).toSet

    info(jobStart, f"total event = ${totalScoreEvents.value}%,d")
    info(jobStart, f"flagged IPs in set: ${ipSet.size}%,d")

    val broadcastedIpSet = sc.broadcast(ipSet)

    def keyValue(key: String, text: String) = {
      val iStart = text.indexOf(key) + 3 + key.length()
      val iFinal = text.indexOf("\"", iStart)
      text.substring(iStart, iFinal)
    }

    // ==============================================================================================
    // 2nd part of the job is to grab additional information from score events for the flagged IPs
    // ==============================================================================================

    val totalScoreFiltered = sc.accumulator(0L)

    // RDD[(json: Json)]
    val filteredScoringEvents = sc.textFile(genFileUrl("s", reportInterval)).filter { str =>
      broadcastedIpSet.value.contains(keyValue("X-LOCM-CIP", str))
    }.map { str =>
      totalScoreFiltered += 1
      val json = parse(str)
      val ip = (json \ "headers" \ "X-LOCM-CIP").values.toString()
      val ua = (json \ "headers" \ "User-Agent").values.toString()
      val id = (json \ "id").values.toString()
      val cookie = (json \ "headers" \ "Cookie").values.toString()
      val uid = "uid=([a-zA-Z0-9-]+)".r.findFirstMatchIn(cookie).map(_.group(1))

      (ip, (ua, id, uid))
    }.persist(StorageLevel.MEMORY_AND_DISK)

    val scoreCount = filteredScoringEvents.countByKey().toMap

    // Array[(ip: String, Map[ua:String, count:Int])]
    val filteredIpUserAgents = filteredScoringEvents.map(evt => (evt._1, evt._2._1)).groupByKey().map(v => (v._1, v._2.groupBy(s => s).mapValues(_.size).map(identity))).collect()

    // Array[(ip: String, Map[:String, count:Int])]    
    val filteredIpUids = filteredScoringEvents.map(evt => (evt._1, evt._2._2)).groupByKey().map(v => (v._1, v._2.groupBy(s => s).mapValues(_.size).map(identity))).collect()

    // Array[(ip: String, Map[ua:String, count:Int])]        
    val filteredIpCookies = filteredScoringEvents.map(evt => (evt._1, evt._2._3)).filter(_._2.isDefined).map(evt => (evt._1, evt._2.get)).groupByKey().map(v => (v._1, v._2.groupBy(s => s).mapValues(_.size).map(identity))).collect()

    val filteredUAs = filteredScoringEvents.map(evt => (evt._2._1, evt._1)).groupByKey().map(v => (v._1, v._2.groupBy(s => s).mapValues(_.size).map(identity))).collect()

    info(jobStart, f"total score events : ${totalScoreFiltered.value}%,d")

    // ==============================================================================================
    // 3rd part of the job is to grab impression data for the flagged IPs
    // ==============================================================================================

    val totalImpressionEvents = sc.accumulator(0L)
    val totalImpressionFiltered = sc.accumulator(0L)

    // Map[ip: String, count: Long]
    val impressionCount = sc.textFile(genFileUrl("i", reportInterval)).filter { str =>
      totalImpressionEvents += 1
      broadcastedIpSet.value.contains(keyValue("X-LOCM-CIP", str))
    }.map { str =>
      val json = parse(str)
      val ip = (json \ "headers" \ "X-LOCM-CIP").values.toString()
      val prd = (json \ "params" \ "prd").values.toString()
      (ip, prd)
    }.filter { x =>
      if (x._2 == "12") {
        totalImpressionFiltered += 1
        true
      } else {
        false
      }
    }.countByKey().toMap

    info(jobStart, f"total impression events : ${totalImpressionEvents.value}%,d")

    // ==============================================================================================
    // 4th part of the job is to grab click data for the flagged IPs
    // ==============================================================================================

    val totalClickEvents = sc.accumulator(0L)
    val totalClickFiltered = sc.accumulator(0L)

    // Map[ip: String, count: Long]
    val clickCount = sc.textFile(genFileUrl("c", reportInterval)).filter { str =>
      totalClickEvents += 1
      broadcastedIpSet.value.contains(keyValue("X-LOCM-CIP", str))
    }.map { str =>
      val json = parse(str)
      val ip = (json \ "headers" \ "X-LOCM-CIP").values.toString()
      val prd = (json \ "params" \ "prd").values.toString()
      (ip, prd)
    }.filter { x =>
      if (x._2 == "12") {
        totalClickFiltered += 1
        true
      } else {
        false
      }
    }.countByKey().toMap

    info(jobStart, f"total click events : ${totalClickEvents.value}%,d")

    // ==============================================================================================
    // final part of the job to generate excel report and deliver it by email
    // ==============================================================================================

    val jobFinal = new DateTime()
    val localHost = InetAddress.getLocalHost()

    val summaryStats: Seq[Any] = Seq(
      s"Report Time Frame: ${str(reportInterval)}",
      None,
      (f"Total scoring events in the last ${hours(reportInterval)} hours:", totalScoreEvents.value.toLong),
      ("Total distinct scoring IPs in the timeframe:", totalDistinctScoringIp.value.toLong),
      (f"Total distinct IPs flagged due to threshold of ${threshold}:", scoreCount.keys.size),
      ("Total scoring events by flagged IPs:", totalScoreFiltered.value.toLong),
      None,
      ("Total impression events in the timeframe:", totalImpressionEvents.value.toLong),
      ("Total flagged IPs producing impressions:", impressionCount.keys.size),
      ("Total impression events by flagged IPs:", totalImpressionFiltered.value.toLong),
      None,
      ("Total click events in the timeframe:", totalClickEvents.value.toLong),
      ("Total flagged IPs producing clicks:", clickCount.keys.size),
      ("Total click events by flagged IPs:", totalClickFiltered.value.toLong))

    val emailBody = summaryStats.map {
      _ match {
        case (text: String, value: Long) => f"${text} ${value}%,d"
        case (text: String, value: Int) => f"${text} ${value}%,d"
        case (text: String, value: String) => f"${text} ${value}"
        case text: String => text
        case None => "\n"
      }
    }.mkString("\n")

    val footer = f"""
        |This report was automatically generated by IPCollusionDetector SparkJob
        |  on $jobFinal
        |  at ${localHost.getHostName()} (${localHost.getHostAddress()})
        |  took ${elapsed(jobStart, jobFinal)} (hh:mm:ss)
        """.stripMargin('|')

    // ==============================================================================================
    // generate excel workbook
    // ==============================================================================================

    val filePath = s"IPCollusion_${jobFinal.toString("yyyy_MM_dd")}.xlsx"

    val sheets = Seq(
      genSummarySheet(threshold, summaryStats, flaggedVisitors),
      genIpSheet(flaggedVisitors.toMap, scoreCount, impressionCount, clickCount, filteredIpUserAgents.toMap, filteredIpCookies.toMap),
      genWorkSheet("IP", "UA", filteredIpUserAgents),
      genWorkSheet("IP", "ID", filteredIpUids),
      genWorkSheet("IP", "Cookie", filteredIpCookies),
      genWorkSheet("UA", "IP", filteredUAs))

    Workbook(sheets: _*).saveAsXlsx(filePath)

    sendEmailWithAttachement(config.getConfig("email"), "IP Collusion Report", f"\n\n${emailBody}\n${footer}", filePath)

    info(jobStart, "finished SparkJob")
  }

}