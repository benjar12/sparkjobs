package service

import util.Settings

import com.mongodb.casbah.Imports._

/**
 * Created by benjarman on 4/29/15.
 * A simple service usually used by
 * higher level services.
 */
class MongoService() {

  private val settings = new Settings().getMongoConf()

  private val mongoClient = MongoClient(settings.host, settings.port)

  private val database = mongoClient(settings.db)

  def getCollection(name: String): MongoCollection ={
    database(name)
  }


  def close(): Unit ={
    mongoClient.close()
  }
}
