package service

import java.io.PrintStream
import java.net.{InetAddress, Socket}

import util.{Logging, Settings}

/**
 * Created by benjarman on 4/28/15.
 */

class GraphiteWriter extends Serializable{

  val logger = new Logging("UpdateGraphiteService.scala")

  //get the settings for this job
  val settings = new Settings().getUpdateGraphiteConf()
  val nameSpace = settings.nameSpace
  val ghost = settings.graphiteHost
  val gport = settings.graphitePort

  //create socket connection
  logger.info(s"Connecting to the following socket: $ghost:$gport")
  val s = new Socket(InetAddress.getByName(ghost), gport)
  val out = new PrintStream(s.getOutputStream())
  val isConnected = s.isConnected
  logger.info(s"IsConnected: $isConnected")

  def write(v: String): Unit ={
    out.println(v)
  }

  def close(): Unit ={
    s.close()
  }

}
