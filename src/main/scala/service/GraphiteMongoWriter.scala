package service

import com.mongodb.casbah.Imports._

/**
 * Created by benjarman on 5/10/15.
 */

/*
  This class is used during the collect
  job. We will take the graphite strings
  and put them into mongo. We will provide
  the collection name. At class creation.
  Batch jobs will use a different collection
  than streaming jobs. Streaming jobs will
  also increment the count. batch will
  overwrite.
  Params :
    collectionName : String
    updateMode : Boolean Default false
   */
class GraphiteMongoWriter(collectionName: String, updateMode: Boolean = false){
  val db = new MongoService
  val coll = db.getCollection(collectionName)

  /*
  If update mode true query mongo,
  get old count and update. Else
  just insert
   */
  def write(id: String, count: Int): Int ={
    val oldCount = if(updateMode){
      val oldR = coll.findOneByID(id)
      if (oldR.isEmpty) {
        0
      } else {
        oldR.get.getAsOrElse[Int]("count", 0)
      }
    }else { 0 }

    val record =
      MongoDBObject(
        "_id" -> id,
        "count" -> (count + oldCount)
      )

    coll.save(record)

    count + oldCount

  }

  /*
  After each job we will close up the connection
  to mongo.
   */
  def close(): Unit ={
    db.close()
  }

}