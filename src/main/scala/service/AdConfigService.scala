package service

import com.mongodb.DBObject
import util.{Logging, Settings}

import scala.collection.immutable.HashMap

/*
This use to be a class, but i'm trying to prevent having to
reload mongo everytime it gets created so i'm experimenting here.

Basic usage :
  val adconfs = AdConfigService().getMap()
  will provide you a hashmap with Ad Config ID
  as the key and the value of Publisher
  case class
  val adconfs = AdConfigService("Domain URL").getMap()
  will do the same except the key of the
  hashmap will now be the domain.
 */
object AdConfigService {

  val logger = new Logging("YpaAdConfigService.scala")

  case class Publisher(adConf: Option[String] = None, publisherName: Option[String] = None, siteGroup: Option[String] = None, domain: Option[String] = None)

  private var adConfigs: AdConfigs = null

  /*
  When called with return either a new instance of
  AdConfigs or return an existing Instance.
   */
  def apply(searchBy: String = "Ad Config ID"): AdConfigs = {

    if (adConfigs == null) {
      logger.debug("Creating new instance of AdConfigs")
      adConfigs = new AdConfigs(searchBy)
    }

    adConfigs

  }

  class AdConfigs(searchBy: String) {
    /*
  Connect to the database
   */
    private val settings = new Settings().getMongoConf()
    private val db = new MongoService
    private val coll = db.getCollection("YPAMapping")

    /*
  Pre-cache our records from mongo into memory
   */
    private val records = coll.find().foldLeft(new HashMap[String, Publisher])((n, o) => {
      if (
          o.containsField("Ad Config ID") &
          o.containsField("Publisher Name") &
          o.containsField("Domain URL") &
          o.containsField("(Y!) Site Group")) {
        val pub = new Publisher(
          Option(o.get("Ad Config ID").toString),
          Option(o.get("Publisher Name").toString.replaceAll(" ", "_")),
          Option(o.get("(Y!) Site Group").toString),
          Option(o.get("Domain URL").toString)
        )

        n.+(o.get(searchBy).toString -> pub)
      } else {
        n
      }
    })

    /*
  Close mongo connection as its no longer needed
   */
    db.close()

    /*
  Provide a method for getting the correct Publisher by what ever searchBy is set to.
  This is better for things that run after collect
   */
    def getPublisher(search: String): Publisher = {
      records.getOrElse(search, new Publisher())
    }
    /*
    Provide a method for pulling out the hash map.
    This is better for things that run before collect
     */
    def getMap(): HashMap[String, Publisher] ={
      records
    }
  }


}