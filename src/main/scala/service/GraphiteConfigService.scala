package service

import com.mongodb.DBObject
import com.mongodb.casbah.Imports._
import com.mongodb.BasicDBList
import com.novus.salat.annotations.Key
import com.novus.salat.dao.SalatDAO
import org.joda.time.DateTime

import com.mongodb.casbah.commons.conversions.scala.{RegisterJodaTimeConversionHelpers, JodaLocalDateTimeSerializer}

import _root_.util.{TimeZoneFactory, Settings}

/**
 * Created by benjarman on 5/6/15.
 * Sample config :
 * {
    "groupBy": [
      ["params",  "prd"],
      ["extparams", "ypaAdConfig", "#publisher"],
      ["extparams", " ypaAdConfig"],
      ["params", "dr"]
    ],
    "filterBy" : [{"index":0, "value": "12"}],
    "nameSpace": ["test","${0}", "publisher_group", "${1}", "${2}", "${3}"]
    "batch": true,
    "streaming": true,
    "topics": ["s"],
    "runFrom": ISODate()
  }
 */

case class GraphiteConfig(
                           _id: ObjectId,
                           groupBy:  List[List[String]], //MongoDBList
                           filterBy: List[(Int, String)],
                           nameSpace: List[String],
                           batchTopics: String,
                           streamingTopics: List[String],
                           runFrom: Option[DateTime]
                           )

object GraphiteConfigService {

  RegisterJodaTimeConversionHelpers()


  private val settings = new Settings().getMongoConf()
  private val collectionName = "GraphiteConfig"

  /*
  Gets us a list of config's that need to be ran by
  either streaming or batch job.
   */
  def apply(toGet: String="batch"): List[GraphiteConfig] ={
    val db = new MongoService
    val coll = db.getCollection(collectionName)

    val query = if(toGet == "batch") {
      MongoDBObject("batch" -> true)
    }else{
      MongoDBObject("streaming" -> true)
    }

    val records = coll.find(
      query
    ).foldLeft(List[GraphiteConfig]())( (l, r) =>{
      /*
      I really hate what this has become. Type
      matching from mongo is a pain and this is
      really ugly code.
       */
      val current = new GraphiteConfig(
        r.getAsOrElse[ObjectId]("_id", new ObjectId),
        (r.getAsOrElse[BasicDBList]("groupBy", new BasicDBList()) collect {case s: BasicDBList => s})
          .foldLeft(List[List[String]]())( (list, next) => {list ++ List( next.collect({case s: String => s})
          .foldLeft(List[String]())( (list, str) => list ++ List(str.toString) ) )} ),
        (r.getAsOrElse[BasicDBList]("filterBy", new BasicDBList()).collect( {case s: BasicDBObject => s}) )
          .foldLeft(List[(Int, String)]())( (list, next) => { list ++ List( ( next.getInt("index"), next.getString("value") ) )})
        ,
        r.getAsOrElse[BasicDBList]("nameSpace", new BasicDBList()).collect( {case s: String => s})
          .foldLeft(List[String]())( (list, next) => {list ++ List(next)}),
        r.getAsOrElse[String]("batchTopics", "null"),
        r.getAsOrElse[BasicDBList]("streamingTopics", new BasicDBList()).collect( {case s: String => s})
          .foldLeft(List[String]())( (list, next) => {list ++ List(next)}),
        r.getAs[DateTime]("runFrom")
      )
      l ++ List(current)
      //l ++ List[GraphiteConfig](grater[GraphiteConfig].asObject(r))
    })

    db.close()

    records

  }

  /*
  After were done processing each batch
  job we will use this method to update
  the next day to be ran in mongo.
   */
  def updateNextDayToRun(config: GraphiteConfig): Unit ={
    val query = MongoDBObject("_id" -> config._id)
    val update = $set("runFrom" -> new DateTime(TimeZoneFactory("UTC")).withTimeAtStartOfDay())
    val db = new MongoService
    val coll = db.getCollection(collectionName)
    val result = coll.update( query, update )
    db.close()
  }

}
