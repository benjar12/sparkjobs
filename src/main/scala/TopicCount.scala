import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.json4s._
import org.json4s.jackson.JsonMethods._

object TopicCount {

  val inputFilePath = "/Users/edholsinger/Desktop/spark-jobs/test.txt"
  val outputFilePath = "/Users/edholsinger/Desktop/spark-jobs/outTopic.txt"

  case class Record(ts: Long, activity: String, params: Params)
  case class Params(prd: Option[String], dr: Option[String])

  def main (args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("DecisionCount"))
    val data = sc.textFile(inputFilePath)

    // Stage 1
    data.map(r => {implicit val format = DefaultFormats; parse(r).extract[Record]})
        .map(r => ((r.ts / 600000, r.params.prd, r.activity).toString(), 1))
        .sortByKey(false)
        .reduceByKey((a, b) => a + b)
        .saveAsTextFile(outputFilePath)
  }
}
