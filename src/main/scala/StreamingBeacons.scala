import com.mongodb.casbah.Imports._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import service.{GraphiteWriter, MongoService}
import util.{Logging, Settings}
import org.apache.spark.streaming.{StreamingContext, _}
import org.apache.spark.streaming.kafka._


/**
 * Created by benjarman on 4/28/15.
 * Sample Usage :
 *  bin/submit.sh StreamingBeacons local[4]
 * This main object is very strait forward.
 * Take data from kafka stream and put it
 * into mongo.
 */
object StreamingBeacons {

  def main(args: Array[String]): Unit = {

    //get a logger
    val logger = new Logging("StreamingBeacons.scala")

    //get settings
    val settings = new Settings().getUpdateGraphiteConf()
    val zkSettings = new Settings().getZookeeperConf()
    val streamSettings = new Settings().getGraphiteStreamingConf()
    val nameSpace = settings.nameSpace

    //create the graphite service
    val service = new GraphiteWriter

    //create streaming context
    val sparkConf = new SparkConf().setAppName("StreamingBeacons.scala")
    val ssc = new StreamingContext(sparkConf, Minutes(2))

    //not sure why im doing this
    ssc.checkpoint("StreamingBeacons")

    //create the stream on the topics we care about
    val topicMap = streamSettings.topics.map((_, 4.toInt)).toMap
    val kafkaStream = KafkaUtils.createStream(
      ssc, zkSettings.hosts, "StreamingBeacons.scala", topicMap
    ).map(_._2)


    kafkaStream.foreachRDD(rdd => {

      def logger = new Logging("StreamingBeacons.scala")
      def db = new MongoService
      //create socket connection
      def socket = new GraphiteWriter()

      //Beacons
      def beacon() {
        logger.info("Starting into Beacons")
        val coll = db.getCollection("Beacons")

        rdd.randomSplit(Array(.25, .25, .25, .25)).foreach(_.collect().foreach(r => {
          try {
            val dat = com.mongodb.util.JSON.parse(r).asInstanceOf[DBObject]
            coll.save(dat)
          }catch{
            case e: Exception => {
              logger.error(s"Bad Record: $r")
              logger.error(e.getMessage)
              val dat = com.mongodb.util.JSON.parse(r.replaceAll(".", "")).asInstanceOf[DBObject]
              coll.save(dat)
            }
          }
        }))
      }

      beacon()

      socket.close()
      db.close()


    })

    ssc.start()
    ssc.awaitTermination()

  }
}
