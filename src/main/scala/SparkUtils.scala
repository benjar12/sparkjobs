/**
 * Created by edholsinger on 3/12/15.
 */

import org.joda.time.{DateTime, Interval}
import org.joda.time.format.{DateTimeFormat, PeriodFormat}

object SparkUtils {

  val DateFormat = "y-M-d"

  val strToDate = (s: String) => DateTimeFormat.forPattern(DateFormat).parseDateTime(s)
  val strToEpoch = (s: String) => strToDate(s).getMillis
  val dateToStr = (d: DateTime) => d.toString(DateFormat)
  val getInterval = (s: String, e: String) => new Interval(strToEpoch(s), strToEpoch(e))
  val epochToYMD = (ts: Long) => new DateTime(ts).toString(DateFormat)

  val getYs = (i: Interval) => i.getStart.getYear until i.getEnd.getYear
  val getMs = (i: Interval) => i.getStart.getMonthOfYear until i.getEnd.getMonthOfYear
  val getDs = (i: Interval) => i.getStart.getDayOfMonth until i.getEnd.getDayOfMonth
}
