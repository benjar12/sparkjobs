package util

/**
 * Created by benjarman on 4/13/15.
 */
import org.joda.time.DateTimeZone

object TimeZoneFactory {
  def apply(timezone: String): DateTimeZone = {
    if (timezone == "UTC") {
      DateTimeZone.UTC
    } else {
      DateTimeZone.forID(timezone)
    }
  }
}

