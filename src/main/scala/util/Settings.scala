package util

import com.typesafe.config.{ConfigValueFactory, Config, ConfigFactory}
import java.util.ArrayList

/**
 * Created by benjarman on 4/27/15.
 *
 */

class Settings(args: Array[String]=Array[String]()) {

  val logger = new Logging(classOf[Settings].getCanonicalName)

  /*
  Config options are able to be overwritten by
  args to the main class
   */
  val argParser = new ArgParser(args)

  /*
  Config case class used mostly by UpdateGraphite.
  This case class is be deprecated.
   */
  case class UpdateGraphiteConf(
                                 graphiteHost: String,
                                 graphitePort: Int,
                                 daysAgo: Int,
                                 StartFrom: Int,
                                 nameSpace: String,
                                 topics   : String,
                                 numOfSplits: Int,
                                 hdfsHost: String,
                                 hdfsBasePath: String,
                                 dailyPath: String,
                                 dateFormat: String,
                                 ext: String,
                                 runTopicCounts: Boolean,
                                 runDecisionCounts: Boolean)

  /*
  Config case class used by service.MongoService
   */
  case class MongoConf(
                      host: String,
                      port: Int,
                      db: String
                        )

  /*
  Config case class used in UpdateGraphite by
  the streaming jobs
   */
  case class ZookeeperConf(
                        hosts: String
                        )

  /*
  Case class that contains hdfs settings
   */
  case class HdfsConf(
                       hdfsHost: String,
                       hdfsBasePath: String,
                       dailyPath: String,
                       dateFormat: String,
                       ext: String
                       )

  /*
  Config case class used in UpdateGraphite
  by StreamingTopicCounts
   */
  case class UpdateGraphiteStreamConf(topics: Array[String])

  /*
  The 2.0 process for updating graphite
  uses this configuration. UpdateGraphiteConf
  is being deprecated
   */
  case class GraphiteConf(
                           batchCollection: String,
                           streamingCollection: String,
                           numOfSplits: Int)

  /*
  Any globals use this conf. It reads
  from the application.conf
   */
  private val conf = ConfigFactory.load()


  /*
  This is for the legacy job, We will
  deprecate this config eventually.
   */
  def getUpdateGraphiteConf(): UpdateGraphiteConf ={
    UpdateGraphiteConf(
      conf.getString("graphite.host"),
      conf.getInt("graphite.port"),
      argParser.get("daysago").getOrElse( conf.getInt("updategraphite.daysago") ).asInstanceOf[Int],
      argParser.get("startfrom").getOrElse( conf.getInt("updategraphite.startfrom") ).asInstanceOf[Int],
      conf.getString("updategraphite.namespace"),
      conf.getString("updategraphite.topics"),
      conf.getInt("updategraphite.numberofsplits"),
      conf.getString("hdfs.host"),
      conf.getString("hdfs.basepath"),
      conf.getString("hdfs.dailypath"),
      conf.getString("hdfs.dateformat"),
      conf.getString("hdfs.ext"),
      argParser.get("tc").getOrElse( conf.getBoolean("updategraphite.runtopiccounts") ).asInstanceOf[Boolean],
      argParser.get("dc").getOrElse( conf.getBoolean("updategraphite.rundecisionscounts") ).asInstanceOf[Boolean]
    )
  }

  /*
  Used by the mongo service
   */
  def getMongoConf(): MongoConf ={
    MongoConf(
      conf.getString("mongo.host"),
      conf.getInt("mongo.port"),
      conf.getString("mongo.db")
    )
  }

  /*
  Used by the streaming jobs
   */
  def getZookeeperConf(): ZookeeperConf = {
    new ZookeeperConf(conf.getString("zookeeper.hosts"))
  }
  def getGraphiteStreamingConf(): UpdateGraphiteStreamConf ={
    new UpdateGraphiteStreamConf(
      conf.getString("updategraphitestream.topics").split(",")
    )
  }

  /*
  This is for the new graphite updating code
  that reads configs from mongo
   */
  def getGraphiteConf(): GraphiteConf ={

    val config = ConfigFactory.load("graphite.conf")

    new GraphiteConf(
      config.getString("batchCollection"),
      config.getString("streamingCollection"),
      config.getInt("numberofsplits")
    )

  }

  def getHdfsConf(): HdfsConf ={
    new HdfsConf(
      conf.getString("hdfs.host"),
      conf.getString("hdfs.basepath"),
      conf.getString("hdfs.dailypath"),
      conf.getString("hdfs.dateformat"),
      conf.getString("hdfs.ext")
    )
  }

  /*
  Used to override config vars based
  on class args.
   */
  def getConfigWithOverrides(name: String): Config ={
    val initConfig = ConfigFactory.load(name)

    argParser.getMap().foldLeft(initConfig)( (conf, next) =>{

      //This gets rid of the ' from the front of the symbol
      val key = next._1.toString().drop(1)
      val value = next._2
      logger.info(s"The following config was overwritten : $key with : $value by the class args")
      conf.withValue(key, ConfigValueFactory.fromAnyRef( value ) )
    })

  }


}
