package util

/**
 * Created by benjarman on 4/13/15.
 * A simple wrapper class for logging
 * in case i would like to add things
 * for thread debugging or something
 * like that
 */
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Logging(name: String){

  private [this] val logger = LoggerFactory.getLogger(name)

  logger.info("New Logger Created")

  def debug(msg: String){logger.debug(msg)}
  def info(msg: String){logger.info(msg)}
  def warn(msg: String){logger.warn(msg)}
  def error(msg: String){logger.error(msg)}
}
