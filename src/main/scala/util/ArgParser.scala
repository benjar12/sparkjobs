package util

/**
 * Created by benjarman on 4/13/15.
 */
class ArgParser(args : Array[String]) {

  val logger = new Logging("ArgParser")

  val arglist = args.toList
  type OptionMap = Map[Symbol, Any]

  //use recursion to parse args
  private[this] def nextOption(map : OptionMap, list: List[String]) : OptionMap = {
    def isSwitch(s : String) = (s(0) == '-')
    list match {
      case Nil => map
        //Generic Args only used by CountTopicByDay right now
      case "-startdate" :: value :: tail =>
        nextOption(map ++ Map('startdate -> value.toString), tail)
      case "-enddate" :: value :: tail =>
        nextOption(map ++ Map('enddate -> value.toString), tail)
      case "-topic" :: value :: tail =>
        nextOption(map ++ Map('topic -> value.toString), tail)
      case "-hdfsbasepath" :: value :: tail =>
        nextOption(map ++ Map('hdfsbasepath -> value.toString), tail)
      case "-filterbyprd" :: value :: tail =>
        nextOption(map ++ Map('filterbyprd -> value.toString), tail)
      case "-dailyorhourly" :: value :: tail =>
        nextOption(map ++ Map('dailyorhourly -> value.toString), tail)
        //Update Graphite Specific Args
      case "-r" :: value :: tail =>
        nextOption(map ++ Map('run -> value.toString), tail)
        //Run topic counts
      case "-tc" :: value :: tail =>
        nextOption(map ++ Map('tc -> value.toBoolean), tail)
        //Run decision counts
      case "-dc" :: value :: tail =>
        nextOption(map ++ Map('dc -> value.toBoolean), tail)
      case "-daysago" :: value :: tail =>
        nextOption(map ++ Map('daysago -> value.toInt), tail)
      case "-startfrom" :: value :: tail =>
        nextOption(map ++ Map('startfrom -> value.toInt), tail)
        //Used by UniqueUsers to override the default config from
        //UniqueUsers.conf
      case "-month" :: value :: tail =>
        nextOption(map ++ Map('month -> value.toInt), tail)
      case "-year" :: value :: tail =>
        nextOption(map ++ Map('year -> value.toInt), tail)
      case option :: tail => logger.warn(s"Unknown arg: $option")
        nextOption(map, tail)
    }
  }
  private val options = nextOption(Map(),arglist)
  logger.info(s"Input Args: $options")

  def getMap(): OptionMap={
    options
  }

  def get(arg: String): Option[Any] ={
    options.get(Symbol(arg))
  }

}
