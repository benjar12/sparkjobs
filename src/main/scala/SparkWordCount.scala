import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import org.json4s._
import org.json4s.jackson.JsonMethods._

import org.joda.time.DateTime

object SparkWordCount {

  val reportYear = 2015
  val reportMonth = 2
  val nextMonth = reportMonth + 1

  val dateTime = new DateTime(reportYear, reportMonth, 1, 0, 0)
  val reportStart = dateTime.getMillis();
  val reportEnd = dateTime.plusMonths(1).getMillis();
  val hdpHost = "la3hdmaster01.epilotcolo.eliberation.com"

  val beaconFileUrl = f"hdfs://$hdpHost%s/user/root/orwell/data/[i|s]/hourly/$reportYear%d/0[$reportMonth%d|$nextMonth%d]/*/*/*.deflate"

  implicit val format = DefaultFormats

  //case class Beacon(id: String, ts: Long, activity: String, headers: Headers)
  //case class Beacon(id: String, ts: Long, activity: String, headers: Map[String, String], params: Params, extparams: ExtParams)
  //case class Params(rid: Option[String], id: Option[String], qs: Option[String], dr: Option[String], prd: Option[String], aff: Option[String], subaff: Option[String], cat: Option[String], subcat: Option[String], src: Option[String], q: Option[String], u: Option[String], r: Option[String], loc: Option[String])
  //case class ExtParams(u: Option[String], ypaAdConfig: Option[String], uid: Option[String], cid: Option[String], ts: Option[String], cts: Option[String], id: Option[String], slotId: Option[String])
  //case class Headers(`X-LOCM-CIP`: String)

  case class Beacon(id: String, ts: Long, activity: String, params: Params)
  case class Params(prd: Option[String])

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf().setAppName("SparkCount"))

    println(f"Using HDFS url as $beaconFileUrl%s")
    val raw = sc.textFile(beaconFileUrl)

                             
    // val raw = sc.textFile("hdfs://la3hdmaster01.epilotcolo.eliberation.com/user/root/orwell/data/[i|s]/hourly/*/*/*/*/*.deflate")
    // val raw = sc.textFile("hdfs://la3hdmaster01.epilotcolo.eliberation.com/user/root/orwell/data/i/hourly/2015/02/25/*/*.deflate")
    // val raw = sc.textFile("hdfs://la3hdmaster01.epilotcolo.eliberation.com/user/root/orwell/data/i/hourly/*/*/*/*/*.deflate")

    val monthImpressions = raw.map(parse(_).extract[Beacon]).filter(b => b.ts > reportStart && b.ts < reportEnd)

    val eitherScoreOrImpressionEvents = monthImpressions.filter(b => (b.params.prd.getOrElse("-1") == "12" && b.activity == "s") || (b.params.prd.getOrElse("12") != "12" && b.activity == "i"))

    val distinctIdsByProduct = eitherScoreOrImpressionEvents.map(b => (b.params.prd, b.id)).distinct()

    distinctIdsByProduct.cache()

    val productCount = distinctIdsByProduct.map(i => (i._1, 1)).reduceByKey((x, y) => x + y)

    val globalCount = distinctIdsByProduct.map(i => i._2).distinct().count()

    println("Product Uniques:")
    productCount.collect().foreach(println)

    println("Global Count = " + globalCount);

    //println(distinctImpressions.count())
    // prds.reduceByKey(a, b => )
    // val tokenized = raw.flatMap(_.split(" "))
    // count the occurrence of each word
    // val wordCounts = tokenized.map((_, 1)).reduceByKey(_ + _)
    // filter out words with less than threshold occurrences
    // val filtered = wordCounts.filter(_._2 >= threshold)

    // count characters
    // val charCounts = filtered.flatMap(_._1.toCharArray).map((_, 1)).reduceByKey(_ + _)

  }

}


