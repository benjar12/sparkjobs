import org.apache.spark.SparkContext._
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Minutes, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.json4s.JsonAST.JValue
import org.json4s._
import org.json4s.jackson.JsonMethods._
import service.AdConfigService.Publisher
import service.{AdConfigService, GraphiteConfigService, GraphiteMongoWriter, GraphiteWriter}
import util.{Logging, Settings, TimeZoneFactory}

/**
 * Created by benjarman on 5/6/15.
 *
 * Sample config :
 * {
    "collectionName": "PublisherCount",
    "groupBy": [
      ["params",  "prd"],
      ["extparams", "ypaAdConfig", "#publisher"],
      ["extparams", "ypaAdConfig"],
      ["params", "dr"]
    ],
    "filterBy" : [{"index":0, "value": "12"}],
    "nameSpace": ["test","${0}", "publisher_group", "${1}", "${2}", "${3}"]
    "batch": true,
    "streaming": true,
    "topics": ["s"]

  }
 */
object UpdateGraphiteStreaming {

  //get our spark context
  val sparkConf = new SparkConf().setAppName("UpdateGraphiteStreaming.scala")
  val ssc = new StreamingContext(sparkConf, Minutes(2))

  //What goes into hdfs for keeping track of offsets
  ssc.checkpoint("Streaming")

  /*
  Entry point to our application.

  Steps :
    1. Fetch configurations from mongo
    2. ForEach config
      B. Build rdd path
      A. Map Job
        a. ForEach groupBy key recursively parse value from the json
        b. If key contains # call runFunction with the last recursive
        value as params
      B. If filter.size > 0
        a. foreach filter based on list index and value
      C. ReduceByKey
      D. Collect.foreach
        a. write to mongo
        b. write to graphite
      E. Update config
   */
  def main(args: Array[String]): Unit = {

    val logger = new Logging("UpdateGraphiteBatch.scala")

    val configs = GraphiteConfigService("streaming")
    val appSettings = new Settings().getGraphiteConf()
    val zkSettings = new Settings().getZookeeperConf()

    //create the stream on the topics we care about
    val topicMap = List[String]("i", "c", "s", "pv").map((_, 4.toInt)).toMap
    val kafkaStream = KafkaUtils.createStream(
      ssc, zkSettings.hosts, "UpdateGraphiteStreaming.scala", topicMap
    ).map(_._2)

    kafkaStream.foreachRDD(rdd => {

      //NotSerializable!!
      val appSettings = new Settings().getGraphiteConf()

      //magic nums
      val divideBy = 600000
      val divideByToGetSeconds = 1000
      val minusSeconds = 1

      //broadcast the adConfigs to all the workers
      val adConfigs = AdConfigService().getMap()

      /*
      Used before the reduceByKey gets the time stamp
      down to hours
       */
      def tsToHours(ts: Long): Long = {
        ts / divideBy
      }

      /*
      Used before writing to graphite and mongo
       */
      def backToTs(hr: Long): Long = {
        ((hr * divideBy) / divideByToGetSeconds) - minusSeconds
      }

      /*
      Used to get publisher from ad config
       */
      def getPublisher(adConfig: String): String = {
        adConfigs.getOrElse(adConfig, new Publisher()).publisherName.getOrElse("null")
      }

      /*
      Used by any groupBy value that has a # as the first
      char.
      We are using the # as a means of calling a function.
      For now publisher is the only one were looking up.
       */
      def runFunction(functionName: String, params: Any): String = {
        if (functionName == "#publisher") {
          getPublisher(params.asInstanceOf[String])
        }
        else {
          "null"
        }
      }

      /*
      takes a json value and a list of keys to extract
      the value. If any of the keys contain # at char
      0 we call run function on the last value.
      Params :
        keys : what we travers EX. ["headers", "X-LOCM-IP"]
        jvalue : The parsed json
        lastValue : the last value extracted
       */
      def recursivelyExtractValue(keys: List[String], jvalue: JValue, lastValue: String = ""): String = {

        val key = keys.head
        val current = jvalue \ key

        val value = if (key.head.equals('#')) {
          runFunction(key, lastValue.asInstanceOf[Any])
        } else {
          current.values.toString
        }

        if (keys.length > 1) {
          recursivelyExtractValue(keys.tail, current, value)
        } else {
          if(value.length > 1){value.replace(".", "_")}
          else{"None"}
        }
      }

      /*
      Params :
        values : List[String] EX. ['12', 'Search_Initiatives', '00000015a', '2']
        nameSpace : List[Any] EX. ['orwell', 1, 'publisher_group', 2, 3, 4]

      Returns : "orwell.12.publisher_group.Search_Initiatives.00000015a.2"
       */
      def recursivelyBuildGraphiteNameSpace(values: List[String], nameSpace: List[Any]): String = {
        val next = nameSpace.head match {
          case s: String => s
          case i: Int => values(i)
        }
        if (nameSpace.size > 1) {
          next + "." + recursivelyBuildGraphiteNameSpace(values, nameSpace.tail)
        } else {
          next
        }
      }

      val logger = new Logging("RDD LOGGER")

      val parsed = rdd.map(r => {
        parse(r)
      })

      configs.foreach(config => {

        /*
        Pre extract the int values from nameSpace so we don't
        Do it on every record. We will then do case matching
        when we use it.
         */
        val nameSpace = config.nameSpace.foldLeft(List[Any]())((list, next) => {
          list ++ List(if (next.charAt(0).equals('$')) {
            next.dropRight(1).drop(2).toInt
          } else {
            next
          })
        })

        val groupBy = config.groupBy
        val filterBy = config.filterBy

        /*
        Run a pre-filter on topic so that we don't process more
        than needed.
         */
        val preFiltered = parsed.filter(r =>{
          config.streamingTopics.contains ( (r \ "activity").values.asInstanceOf[String] )
        })

        /*
        The Tuple Produced here is as follows
        ( ( List[String], ts: Long), 1)
        We may have to change that to
        List[Any]
         */
        val mapped = preFiltered.map(r => {

          implicit val format = DefaultFormats
          val tsOg = (r \ "ts").values.asInstanceOf[BigInt].toLong
          val ts = if (tsOg != 0L) {
            tsToHours(tsOg)
          } else {
            tsOg
          }

          def gbs = groupBy

          val tp = (
            (
              gbs.foldLeft(List[String]())((list, next) => {
                list ++ List(recursivelyExtractValue(next, r))
              }),
              ts
              ),
            1)

          tp
        })

        /*
        Filter anything that has 0L ts or doesn't
        match what is in mongo
         */
        val filtered = mapped.filter(r => {
          val fl = filterBy
          r._1._2 != 0L && fl.foldLeft(true)((shouldBeIn, next) => {

            val index = next._1
            val eq = next._2
            val to = r._1._1(index)

            if (eq != to) {
              false
            }
            else {
              shouldBeIn
            }
          })
        })

        /*
        Get counts of everything that matches
        r._1
         */
        val reduced = filtered.reduceByKey((a, b) => a + b)

        /*
        Convert timestamps back from the 10
        minute grouping and flatten the
        tuple
         */
        val reMapped = reduced.map(r => {
          (r._1._1, backToTs(r._1._2), r._2)
        })

        /*
        Split the rdd for processing
         */
        val split = reMapped.randomSplit(Array.fill[Double](4)(1.toDouble / 4))

        /*
        Collect
        1. Build NameSpace
        2. write to mongo
        3. write to graphite
         */
        split.foreach(rdd => {

          //Create our mongo Writer and Socket Connection to graphite
          val mongoWriter = new GraphiteMongoWriter(appSettings.streamingCollection, true)
          val graphiteSocket = new GraphiteWriter()

          rdd.collect().foreach(r => {

            //Create our namespace and extract
            //other things we need.
            val ts = r._2
            val count = r._3
            val ns = recursivelyBuildGraphiteNameSpace(r._1, nameSpace) + s" %s $ts"

            //write to mongo
            val updateCount = mongoWriter.write(ns, count)

            val formattedNs = ns.format(updateCount.toString)

            logger.info(s"Writting the following to graphite: $formattedNs")

            //write to graphite
            graphiteSocket.write(formattedNs)

          })

          //clean up
          mongoWriter.close()
          graphiteSocket.close()
        })

      }) //END OF FOREACH CONFIG


    }) //END OF FOREACH RDD

    ssc.start()
    ssc.awaitTermination()


  } //END OF MAIN

}

//END OF OBJECT
