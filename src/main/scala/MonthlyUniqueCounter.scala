import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import org.json4s._
import org.json4s.jackson.JsonMethods._

import org.joda.time.DateTime

object MonthlyUniqueCounter {

  case class Beacon(id: String, ts: Long, activity: String, params: Params)
  case class Params(prd: Option[String])

  def main(args: Array[String]) {

    val reportYear = 2015
    val reportMonth = 2
    val nextMonth = reportMonth + 1

    val dateTime = new DateTime(reportYear, reportMonth, 1, 0, 0)
    val reportStart = dateTime.getMillis()
    val reportEnd = dateTime.plusMonths(1).getMillis()
    val hdpHost = "la3hdmaster01.epilotcolo.eliberation.com"

    val remoteFileUrl = f"hdfs://$hdpHost%s/user/root/orwell/data/[i|s]/hourly/$reportYear%d/0[$reportMonth%d|$nextMonth%d]/*/*/*.deflate"
    val localFileUrl = f"/user/root/orwell/data/[i|s]/hourly/$reportYear%d/0[$reportMonth%d|$nextMonth%d]/*/*/*.deflate"

    val sc = new SparkContext(new SparkConf().setAppName("MonthlyUniqueCounter"))
    val raw = sc.textFile(localFileUrl)

    // Stage 1
    val monthImpressions = raw.map({implicit val format = DefaultFormats; parse(_).extract[Beacon]}).filter(b => b.ts > reportStart && b.ts < reportEnd)
    val eitherScoreOrImpressionEvents = monthImpressions.filter(b => (b.params.prd.getOrElse("-1") == "12" && b.activity == "s") || (b.params.prd.getOrElse("12") != "12" && b.activity == "i"))
    val distinctIdsByProduct = eitherScoreOrImpressionEvents.map(b => (b.params.prd, b.id)).distinct()
    distinctIdsByProduct.cache()

    // Stage 2
    val productCount = distinctIdsByProduct.map(i => (i._1, 1)).reduceByKey((x, y) => x + y)

    // Stage 3
    val globalCount = distinctIdsByProduct.map(i => i._2).distinct().count()

    // Stage 4
    println("Product Uniques:")
    productCount.collect().foreach(println)
    println("Global Count = " + globalCount)
  }

}


