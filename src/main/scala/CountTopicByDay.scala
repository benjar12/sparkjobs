/**
 * Created by benjarman on 4/13/15.
 */

import com.sun.media.sound.InvalidDataException
import org.apache.hadoop.mapred.InvalidInputException
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._
import util.{Logging, ArgParser, TimeZoneFactory}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import org.apache.spark._
import org.apache.spark.SparkContext._

object CountTopicByDay {

  val usage =
    """
      | A sample usage would look like:
      | $SPARK_HOME/bin/submit \
      | --class CountTopicByDay \
      | --master yarn-client \
      | --num-executors 3 \
      | --driver-memory 512m \
      | --executor-memory 512m \
      | target/scala*/spark-jobs*.jar "-topic i -startdate 2015/02/02 -enddate 2015/02/03"
      |
      | An example with daily folder is:
      | ~/DEV_HDP_CONF/run_scala_job.sh \
      | target/scala-2.10/spark-jobs-assembly-1.0.jar \
      | CountTopicByDay \
      | "local[2]" \
      | "-hdfsbasepath hdfs://la3devmaster01/user/root/orwell/data -topic demo -startdate 2015/04/01 -enddate 2015/04/13 -filterbyprd 12 -dailyorhourly daily"
    """.stripMargin

  val logger = new Logging("CountTopicByDay.scala")

  case class Record(ts: Long, activity: String, params: Params)
  case class Params(prd: Option[String], dr: Option[String])

  def main(args: Array[String]): Unit ={

    //parse args
    val argParser = new ArgParser(args)
    val start = argParser.get("startdate").getOrElse("2015/04/02").toString
    val end = argParser.get("enddate").getOrElse("2015/04/13").toString
    val hdfs = argParser.get("hdfsbasepath").getOrElse("hdfs://la3hdmaster01/user/root/orwell/data").toString
    val topic = argParser.get("topic").getOrElse("c").toString
    val filterOnPrd = argParser.get("filterbyprd").getOrElse("-1").toString
    val folder = argParser.get("dailyorhourly").getOrElse("hourly").toString
    val inputDateFormat = "yyyy/MM/dd"

    //handle the diff in hourly & daily folder paths
    val dateformat =
      if(folder == "hourly"){
        "yyyy/MM/dd/*/*"
      }else{
        "yyyy/yyyy-MM-dd*"
      }

    //create spark context
    val sparkConf = new SparkConf().setAppName("CountTopicByDay")
    val sc = new SparkContext(sparkConf)

    //create date objects from strings
    val startDate = DateTime.parse(start, DateTimeFormat.forPattern(inputDateFormat))
    val endDate = DateTime.parse(end, DateTimeFormat.forPattern(inputDateFormat))

    logger.info(s"Going to count from: $start to $end")

    //iterate of dates in range
    def countDay(current: DateTime): Array[(DateTime, Long)] ={

      try {

        val curString = DateTimeFormat.forPattern(dateformat).print(current)
        val isBefore = current.isBefore(endDate.plus(1))
        logger.info(s"$curString < $end == $isBefore")
        //check that we are in range
        if (isBefore) {

          //create the file path
          val file = sc.textFile(s"$hdfs/$topic/$folder/$curString")

          val f = if(folder == "daily"){file.repartition(4)}else{file}

          //if filter on prd parse and filter
          val count = if(filterOnPrd != "-1") {
            f.map(r => {
              implicit val format = DefaultFormats; parse(r).extract[Record]
            })
              .filter(r => r.params.prd.getOrElse("") == filterOnPrd)
              .count()
          //If not just count
          }else{
            f.count()
          }
          //repeat until we hit a day we dont want to process
          Array[(DateTime, Long)]((current, count)) ++ countDay(current.plusDays(1))

        } else {
          Array[(DateTime, Long)]()
        }
      }catch{

        case e: InvalidDataException => {
          logger.error(e.getMessage)
          Array[(DateTime, Long)]()
        }
        case e: InvalidInputException => {
          logger.error(e.getMessage)
          countDay(current.plusDays(1))
        }
        case e: Exception =>{
          logger.error(e.getMessage)
          System.exit(1)
        }

          Array[(DateTime, Long)]()

      }
    }

    val counts = countDay(startDate)

    counts.foreach(tp => {
      val curString = DateTimeFormat.forPattern(dateformat).print(tp._1)
      val count = tp._2
      println(s"Day: $curString Count: $count")
    })

  }
}
