import com.norbitltd.spoiwo.model._
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._

import com.norbitltd.spoiwo.model._
import com.norbitltd.spoiwo.model.enums.CellFill
import com.norbitltd.spoiwo.natures.xlsx.Model2XlsxConversions._

/**
 * Created by benjarman on 4/16/15.
 */
object IPCount {

  case class Record(ts: Long, activity: String, params: Params)
  case class Params(prd: Option[String], dr: Option[String], aff: Option[String])

  def main(args: Array[String]): Unit ={
    //create spark context
    val sparkConf = new SparkConf().setAppName("CountTopicByDay")
    val sc = new SparkContext(sparkConf)
    val filePath = "hdfs://la3hdmaster01/user/root/orwell/deprecated/i/hourly/2015/03/*/*/*"
    val prd = "14"

    val file = sc.textFile(filePath)

    val mapped = file.map(r => {
        implicit val format = DefaultFormats;
        ( parse(r).extract[Record].params.prd.getOrElse(""),
          parse(r) \ "headers" \ "X-LOCM-CIP" ,
          parse(r).extract[Record].params.aff.getOrElse("")
          )
      })
        .filter(r => {r._1 == prd})


    val counts = mapped.map(r => ( (r._2, r._3), 1) ).reduceByKey((x, y) => x + y).filter(f => {f._2 >= 20})
    //val counts = mapped.map(r => ( r._2, (r._3, 1) ) ).reduceByKey((x, y) => (x._1, x._2 + y._2) )

    val ipSheet = Sheet(
      name = "Ip Aff",
      Row().withCellValues("IP", "Aff", "Count"))
      .withColumns(Column(index = 0, style = CellStyle(font = Font(bold = true)), autoSized = true))
      .addRows(for (c <- counts.collect()) yield Row().withCellValues(c._1._1.values.toString, c._1._2, c._2))
    Workbook(ipSheet).saveAsXlsx("IPCounts.xlsx")

  }

}
