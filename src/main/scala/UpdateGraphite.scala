import util.{ArgParser, Settings, Logging, TimeZoneFactory}

import com.mongodb.casbah.MongoCollection
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming._
import org.joda.time.format.DateTimeFormat
import org.json4s._
import org.json4s.jackson.JsonMethods._

import org.joda.time.DateTime

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._

import java.net._
import java.io._
import service.AdConfigService.{AdConfigs, Publisher}
import service.{AdConfigService, GraphiteWriter, MongoService}

import scala.collection.immutable.HashMap

import scala.io._

import com.mongodb.casbah.Imports._

/**
 * Created by benjarman on 4/27/15.
 *
 * Sample Usage :
 *  bin/submit.sh UpdateGraphite local[4] "-r batch -tc false -dc true -startfrom 0 -daysago 1"
 *  bin/submit.sh UpdateGraphite local[4] "-r streamingTopics"
 *  bin/submit.sh UpdateGraphite local[4] "-r streamingDecisions"
 */
object UpdateGraphite {

  /*
  For now this has to be here. The case
  classes need the name space, how ever
  I need the args to the class for the
  rest of the settings. If i move the
  case classes into the main method
  the job does not run.
   */
  val nameSpace = new Settings().getUpdateGraphiteConf().nameSpace

  //magic nums
  val divideBy = 600000
  val divideByToGetSeconds = 1000
  val minusSeconds = 1

  /*
  Used before the reduceByKey gets the time stamp
  down to hours
   */
  def tsToHours(ts: Long): Long = {
    ts / divideBy
  }

  /*
  Used before writing to graphite and mongo
   */
  def backToTs(hr: Long): Long = {
    ( (hr * divideBy) / divideByToGetSeconds) - minusSeconds
  }

  /*
  Our case classes that extract the data from
  the json
   */
  case class Record(ts: Long, activity: String, params: Params, extparams: ExtParams)
  case class Params(prd: Option[String], dr: Option[String])
  case class ExtParams(ypaAdConfig: Option[String])

  /*
  Our key during the reduce operation for
  decisions aggregation
   */
  case class Decisions(ts: Long, prd: String, dr: String, adConfig: String, publisher: String) extends Serializable{
    /*
    This graphite string is not broken out by adconfigs and
    should eventually be killed. This should only be run after
    we have removed the adconfig or else we will have double data.
     */
    def toGraphiteString(count: Int): String ={
      val fixedTs = backToTs(ts)
      s"$nameSpace.$prd.decision.$dr $count $fixedTs"
    }
    def instanceWithoutPublisherInfo(): Decisions = {
      new Decisions(ts, prd, dr, "N/A", "N/A")
    }
    /*
    This to String will further break things out
     */
    def toPublisherGraphiteString(count: Int): String={
      val fixedTs = backToTs(ts)
      s"$nameSpace.$prd.publisher_group.$publisher.$adConfig.$dr $count $fixedTs"
    }
  }

  /*
  Our key during the reduce operation for
  topic counts
   */
  case class Topics(ts: Long, activity: String, prd: String) extends Serializable{
    def toGraphiteString(count: Int): String ={
      val tsFixed = backToTs(ts)
      val topic = activity
      s"$nameSpace.$prd.topicStat.$topic $count $tsFixed"
    }
  }

  /*
  So this class has 3 modes batch, streamingTopics, and streamingDecisions
  I really dont like that all of this logic has to be in one main method but it seems to be
  only way to get good code reuse. The are -r stands for run, so if you pass -r batch it will
  run the batch jobs. If you pass -r streamingTopics it will run the steaming topics job etc.
   */
  def main(args: Array[String]): Unit = {

    val argParser = new ArgParser(args)
    val toBeRan = argParser.get("run").getOrElse("batch")

    //get the settings for this job
    val settings = new Settings(args).getUpdateGraphiteConf()
    val zkSettings = new Settings(args).getZookeeperConf()
    val streamSettings = new Settings(args).getGraphiteStreamingConf()
    val daysAgo = settings.daysAgo
    val yesterday = DateTime.now(TimeZoneFactory("UTC")).minusDays(1).minusDays(settings.StartFrom)
    val hdfs = settings.hdfsHost + settings.hdfsBasePath
    val dailyPath = settings.dailyPath
    val nameSpace = settings.nameSpace
    val topics = settings.topics
    val ghost = settings.graphiteHost
    val gport = settings.graphitePort

    //get a logger
    val logger = new Logging("UpdateGraphite.scala")

    logger.info("running with the following settings: " + settings)

    logger.info(s"Using the following hdfs path: $hdfs")

    //connect to mongo
    val db = new MongoService

    //get our spark context
    val sparkConf = new SparkConf().setAppName("UpdateGraphite.scala")


    /*
    The main method for running Decision Counts
    This method will have two modes update and none
    update. In update mode we will read in the old
    value from mongo and rewrite it out. This is
    used for streaming, while none update mode wil
    be used for batch.
     */
    def runDecisions(
                      rdd: RDD[String],
                      adConfs: HashMap[String, Publisher],
                      dbs: List[MongoCollection],
                      updateMode: Boolean=false): Unit ={

      //get a logger
      val logger = new Logging("UpdateGraphite.scala")

      logger.info("Before Map Job")

      //create socket connection
      val socket = new GraphiteWriter()

      /*
      Parse Json and map the data to a Decisions CaseClass
       */
      val mapped = rdd.map(str => {

        try {
          implicit val format = DefaultFormats
          def r = parse(str).extract[Record]
          val adconfig = r.extparams.ypaAdConfig.getOrElse("null")
          (new Decisions(
            tsToHours(r.ts),
            r.params.prd.getOrElse("null"),
            r.params.dr.getOrElse("null"),
            adconfig,
            adConfs.getOrElse(adconfig, new Publisher()).publisherName.getOrElse("null")
          ), 1)
        }catch{
          case e: Exception => {
            println(s"Bad Record: $str")
            println(e.getMessage)
            sys.exit(1)
          }
        }
      })

      logger.info("Before Filter Job")

      /*
      Make sure we don't have any null decisions
       */
      val filtered = mapped.filter(r => {
        r._1.dr != "null"
      })

      /*
      We really only need this function for code reuse while
      we still have to support legacy stuff. this is the logic
      for the map.collect.foreach( ... )
       */
      def process(r: (Decisions, Int), coll: MongoCollection){

        logger.info("In Processing")

        //set vars
        val ogTs = r._1.ts
        val ts = backToTs(ogTs)
        val prd = r._1.prd
        val dr = r._1.dr
        val adConf = r._1.adConfig
        val count = r._2
        val mongoId = s"$prd::$adConf::$dr::$ogTs"

        //query mongo for record
        val oldR = coll.findOneByID(mongoId)
        val oldCount = if (!updateMode || oldR.isEmpty) {
          0
        } else {
          oldR.get.getAsOrElse[Int]("count", 0)
        }
        val updateCount = if (oldR.isEmpty) {
          0
        } else {
          oldR.get.getAsOrElse[Int]("updateCount", 0)
        } + 1

        val updatedCount = oldCount + count

        //This is just for legacy stuff, eventually we wont need it.
        val toGraphite = if(adConf != "N/A") {
          r._1.toPublisherGraphiteString(updatedCount)
        }else{
          r._1.toGraphiteString(updatedCount)
        }
        logger.info(s"Sending the following to graphite: $toGraphite")

        logger.info(s"Last count: $oldCount, updated count: $updatedCount for id: $mongoId")

        val record =
          MongoDBObject(
            "_id" -> mongoId,
            "dr" -> dr,
            "prd" -> prd,
            "adConf" -> adConf,
            "count" -> updatedCount,
            "ts" -> ts,
            "updateCount" -> updateCount,
            "graphiteValue" -> toGraphite
          )

        socket.write(toGraphite)
        coll.save(record)
      }

      logger.info("Before Reduce Job")

      /*
      The reduce operation for events broken out by adConfig
       */
      val reduced = filtered.reduceByKey((a, b) => a + b)
      reduced.randomSplit(Array.fill[Double](settings.numOfSplits)(1.toDouble / settings.numOfSplits)).foreach(
        _.collect()
          .foreach(r => {
            process(r, dbs(0))
        }))
      /*
      The re-map and reduce operation for events broken out just by
      dr
       */
      val legacyReduced = filtered.map(r => {
        ( r._1.instanceWithoutPublisherInfo(), 1)
      }).reduceByKey((a, b) => a + b)
      legacyReduced.randomSplit(Array.fill[Double](settings.numOfSplits)(1.toDouble / settings.numOfSplits)).foreach(
        _.collect()
          .foreach(r => {
          process(r, dbs(1))
        }))


      socket.close()

    }

    /*
    This method will be used for both the streaming and
    the batch topicCount job. It will take in an rdd and
    update mongo as well as graphite. If update mode is false
    mongo records will be overwritten if update mode is true
    the count will be incremented with the new count. Update
    mode is strictly for streaming batch should never use it.
     */
    def runTopics(
      rdd: RDD[String],
      coll: MongoCollection,
      updateMode: Boolean=false): Unit ={

      //get a logger
      val logger = new Logging("UpdateGraphite.scala")

      /*
      Create our socket connection to graphite
       */
      val socket = new GraphiteWriter

      /*
      Map the data to our topic case class
       */
      val mapped = rdd.map(str=>{
        implicit val format = DefaultFormats
        def r = parse(str).extract[Record]
        (
          new Topics(
            tsToHours(r.ts),
            r.activity,
            r.params.prd.getOrElse("null")
          ),
          1)
      })
      /*
      Reduce by key to get our counts
       */
      val reduced = mapped.reduceByKey((a, b) => a + b)

      /*
      Process the records, update mongo and update graphite
      Steps:
      1. Split the RDD based on what is in the config file
      2. On each split collect
      3. For each in split
        a. Create mongoid
        b. query mongo
        c. if updatemode and record found increment count
        d. write to mongo
        e. write to graphite
       */
      reduced.randomSplit(Array.fill[Double](settings.numOfSplits)(1.toDouble / settings.numOfSplits)).foreach(
        _.collect()
          .foreach(r => {
          //set vars
          val ogTs = r._1.ts
          val ts = backToTs(ogTs)
          val topic = r._1.activity
          val prd = r._1.prd
          val count = r._2
          val mongoId = s"$topic::$prd::$ogTs"

          //query mongo for record
          val oldR = coll.findOneByID(mongoId)
          val oldCount = if (!updateMode || oldR.isEmpty) {
            0
          } else {
            oldR.get.getAsOrElse[Int]("count", 0)
          }
          val updateCount = if (oldR.isEmpty) {
            0
          } else {
            oldR.get.getAsOrElse[Int]("updateCount", 0)
          } + 1

          val updatedCount = oldCount + count

          logger.info(s"Last count: $oldCount, updated count: $updatedCount for id: $mongoId")

          //create the graphite string
          val toGraphite = s"$nameSpace.$prd.topicStat.$topic $updatedCount $ts"

          //if none create
          val record =
            MongoDBObject(
              "_id" -> mongoId,
              "topic" -> topic,
              "prd" -> prd,
              "count" -> updatedCount,
              "ts" -> ts,
              "updateCount" -> updateCount,
              "graphiteValue" -> toGraphite
            )

          //save to mongo
          coll.save(record)

          //write to graphite
          logger.info(s"Sending the following to graphite: $toGraphite")
          socket.write(toGraphite)
        }))

      /*
      Clean up
       */
      socket.close()

    }

    /*
    This is where the actual batch job logic lives.
     */
    if(toBeRan == "batch") {

      val sc = new SparkContext(sparkConf)

      //get our mapping. We use this for s events to
      //map our ypa adconfig to a pretty name such as
      //space jet
      val adConfs = sc.broadcast(AdConfigService().getMap())

      /*
    build are rdds. This function used recursion to
    build a Sequence of RDDs. It reads the daysago
    and startfrom params from the config file to
    decide on how many days it should add to the Seq.
     */
      def build(current: DateTime, dr: Boolean = false): Seq[RDD[String]] = {
        val next = current.minusDays(1)
        val c = DateTimeFormat.forPattern(settings.dateFormat).print(current) + settings.ext
        val t = if (dr == true) "s" else topics
        logger.info(s"$c for topics: $t added to the rdd, with dr option set to : $dr")
        if (next.isAfter(yesterday.minusDays(daysAgo))) {
          try {

            Seq(sc.textFile(s"$hdfs/$t/$dailyPath/*/$c")) ++ build(next, dr)
          } catch {
            case e: Exception => {
              logger.error(e.getMessage)
              build(next, dr)
            }
          }
          //Array[String]( c )
        } else {
          try {
            //Array[String]( c )
            Seq(sc.textFile(s"$hdfs/$t/$dailyPath/*/$c"))
          } catch {
            case e: Exception => {
              logger.error(e.getMessage)
              Seq()
            }
          }
        }
      }

      val runTc = settings.runTopicCounts
      val runDc = settings.runDecisionCounts
      logger.info(s"SETTINGS FOR WHAT TO RUN TC: $runTc DC: $runDc")

      if (runTc) {

        logger.info("Preparing to run topic counts")

        //create our big rdd
        val tctxt = sc.union(build(yesterday))

        //get the collection for this job
        val coll = db.getCollection("batchTopicCountsV2")

        runTopics(tctxt, coll)
      }

      if (runDc) {

        logger.info("Preparing to run decision counts")

        val drtxt = sc.union(build(yesterday, true))

        //get the collection for this job
        val coll = db.getCollection("batchPublisherDecisionCounts")
        val coll2 = db.getCollection("batchDecisionCountsV2")

        runDecisions(drtxt, adConfs.value, List(coll, coll2))


      }

    }

    /*
      This is our logic for streaming decision counts
       */
    if(toBeRan == "streamingDecisions"){

      val ssc = new StreamingContext(sparkConf, Minutes(2))

      //What goes into hdfs for keeping track of offsets
      ssc.checkpoint("StreamingDecisionsV2")

      //create the stream on the topics we care about
      val topicMap = List[String]("s").map((_, 3.toInt)).toMap
      val kafkaStream = KafkaUtils.createStream(
        ssc, zkSettings.hosts, "StreamingDecisions.scala", topicMap
      ).map(_._2)

      val adConf = AdConfigService().getMap()

      kafkaStream.foreachRDD(rdd => {

        //get a logger
        val logger = new Logging("UpdateGraphite.scala")

        logger.info("Before db service")

        val db = new MongoService

        logger.info("Before collection grabbing")

        //get the collection for this job
        val coll = db.getCollection("streamingPublisherDecisionCounts")
        val coll2 = db.getCollection("streamingDecisionCountsV2")

        logger.info("After collection grabbing")

        runDecisions(rdd, adConf, List(coll, coll2), true)

        logger.info("After decisions run")

        db.close()


      })

      ssc.start()
      ssc.awaitTermination()

    }

    /*
      This is our logic for streaming topic counts
       */
    if(toBeRan == "streamingTopics"){

      val ssc = new StreamingContext(sparkConf, Minutes(2))

      //What goes into hdfs for keeping track of offsets
      ssc.checkpoint("StreamingTopicsV2")

      //create the stream on the topics we care about
      val topicMap = streamSettings.topics.map((_, 4.toInt)).toMap
      val kafkaStream = KafkaUtils.createStream(
        ssc, zkSettings.hosts, "StreamingTopicCounts.scala", topicMap
      ).map(_._2)


      kafkaStream.foreachRDD(rdd => {

        //get a logger
        val logger = new Logging("UpdateGraphite.scala")

        logger.info("Before db service")

        val db = new MongoService

        logger.info("Before collection grabbing")

        //get the collection for this job
        val coll = db.getCollection("streamingTopicCountsV2")

        logger.info("After collection grabbing")

        //run topic counts in update mode
        runTopics(rdd, coll, true)

        logger.info("After topics run")

        db.close()


      })

      ssc.start()
      ssc.awaitTermination()

    }

    //close our db connection
    db.close()


  }

}
