/** Retrieves counts of Monthly Unique Visitors
  *
  */

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import org.json4s._
import org.json4s.jackson.JsonMethods._

import scalikejdbc._

import SparkUtils.epochToYMD

import math.min
import com.typesafe.config.ConfigFactory
import com.microsoft.sqlserver.jdbc.SQLServerException

import util.{Settings, ArgParser}

object UniqueUsers extends App {

    case class Headers(Cookie: Option[String], `User-Agent`: Option[String])

    case class Params(prd: Option[String])

    case class Record(ts: Long, id: String, headers: Headers, params: Params)

    /** Structure defining our pivot values and method of combination */
    case class ValueSet(prd: String, ts: Long, ret: Boolean, bot: Boolean, dev: String)

    def combineValueSets(a: ValueSet, b: ValueSet): ValueSet = {
      ValueSet(a.prd,
        min(a.ts, b.ts),
        a.ret && b.ret,
        a.bot && b.bot,
        if (a.dev.contains(b.dev)) a.dev else a.dev + b.dev)
    }

    /** Structure converting our pivot values into a map-reduce key */
    case class ValueSetKey(prd: String, date: String, ret: Int, bot: Int, dev: String)

    def ValueSetToKey(v: ValueSet): ValueSetKey =
      ValueSetKey(v.prd, epochToYMD(v.ts), if (v.ret) 1 else 0, if (v.bot) 1 else 0, v.dev)

    /** Convenience structure for writing out to SQL or CSV */
    case class Datum(prd: String, window: String, date: String, ret: Int, bot: Int, dev: String, count: Long) {
      def this(values: ValueSetKey, window: String, count: Long) =
        this(values.prd, window, values.date, values.ret, values.bot, values.dev, count)

      override def toString = Array(this.prd, this.window, this.date, this.ret, this.bot, this.dev, count).mkString(", ")
    }

  override def main(args: Array[String]) {

    /* So what this does is grab the commandline args. Anything that
     * is defined In ArgParser can override values we can now run
     * spark-submit ... spark-jobs*.jar '-month 5 -year 2015'
     * in addition to that if we want to have global configs we
     * can use application.conf as a fallback config. That will come
     * later after we have a standard config ~Ben*/
    val conf = new Settings(args).getConfigWithOverrides("UniqueUsers")

    /** Helper Function: Checks to see if an event is new or a return visit */
    def isReturn = (id: String, cookie: Option[String]) => cookie.getOrElse("") contains id

    /** Determines whether a given visitor is a bot or not. */
    // TODO: Implement Bot Detection
    def isBot = (ua: Option[String]) => false

    /** Determines the device of a given visitor from the User Agent String
      *
      * - D = Desktop
      * - M = Mobile
      * - T = Tablet
      * */
    // TODO: Implement Device Detection
    def getDevice = (ua: Option[String]) => "D"

    /** Establish File Selection
      *
      * Note that we're not specifying any filters on the Epoch time here. This is intentional. The new
      * file format should allow for file selection to be a sufficient filter. If this is not the case, then
      * we don't want to mask the problem by adding additional filtering logic.
      * */
    val year = conf.getInt("year")
    val month = conf.getInt("month")
    val host = conf.getString("hdp.host")
    // val inputFilePath = (a: String) => f"hdfs://$host/user/root/orwell/data/$a/daily/2015/2015-04-09.bz2"
    val inputFilePath = (a: String) => f"hdfs://$host/user/root/orwell/data/$a/daily/$year/$year-$month%02d-*.bz2"
    // val inputFilePath = (a: String) => f"file:///Users/edholsinger/desktop/hdfs/user/root/orwell/data/s/daily/2015/*.bz2"

    /** Establish RDDs for each topic that we need and merge them.
      *
      * To count Uniques reliably we need to count unique events. For most products Impressions are unique in that
      * there is a 1 to 1 correspondence between a user session and an impression. This is not the case for product 12,
      * and is rapidly becoming an issue for other products. Right now there are three places to look for unique events:
      *
      * - For Products 1, 2 and 14 use Impressions prior to the change over to Page Views, use Page Views after
      * - For Product 12 use Scoring events, as there are no Page Views and multiple impressions are generated per session
      * - For everything else use impressions
      *
      * Luckily the transition from Impressions to Page Views is just a name change. So the assumption is that
      * Impressions + Page Views in the new naming scheme is equivalent to Impressions in the old naming scheme, thus
      * the merge is entirely straightforward.
      * */
    val sc = new SparkContext(new SparkConf().setAppName("MUV"))

    /** Stage 1a: Get Scoring Event Data Selection (for PRD 12) */
    val sData = sc.textFile(inputFilePath("s"))
    val sPageViews = sData.map(r => {
      implicit val format = DefaultFormats
      //      try {
      parse(r).extract[Record]
      //      } catch {
      //        case e: Exception => throw new Exception(r)
      //      }
    })
      .filter(r => r.params.prd.getOrElse("") == "12")

    /** Stage 1b: Impression + Page View Data Selection (For non PRD 12) */
    val iData = sc.textFile(Array(inputFilePath("i"), inputFilePath("pv")).mkString(","))
    val iPageViews = iData.map(r => {
      implicit val format = DefaultFormats
      //      try {
      parse(r).extract[Record]
      //      } catch {
      //        case e: Exception => throw new Exception(r)
      //      }
    })
      .filter(r => r.params.prd.getOrElse("") != "12")

    /** Stage 2: Join Into One RDD */
    val pageViews = sPageViews ++ iPageViews

    /** Stage 3: Compute Monthly Uniques */
    val monthly = pageViews.map(r => {
      try {
        (
          (r.id, r.params.prd.getOrElse("NA")),
          ValueSet(r.params.prd.getOrElse("NA"),
            r.ts,
            isReturn(r.id, r.headers.Cookie),
            isBot(r.headers.`User-Agent`),
            getDevice(r.headers.`User-Agent`)
          )
          )
      } catch {
        case e: Exception => throw new Exception(isReturn(r.id, r.headers.Cookie).toString)
      }
    }
    )
      .reduceByKey(combineValueSets)
      .map(r => (ValueSetToKey(r._2), 1))
      .reduceByKey(_ + _)
      .map(r => new Datum(r._1, "M", r._2))

    /** Stage 4: Write Data */
    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
    ConnectionPool.singleton(conf.getString("db.server"), conf.getString("db.user"), conf.getString("db.pwd"))

    // TODO: bot & device set to null until implementation
    def loadSQL(data: Datum, db: String): Unit = {
      implicit val session = AutoSession

      try {
        sql"""Insert INTO MUV.dbo.MUV (ProductID, Window, Date, ReturnVisit, Bot, DeviceType, "Count")
              VALUES (${data.prd}, ${data.window}, ${data.date}, ${data.ret}, ${null}, ${null}, ${data.count})
         """.update().apply()
      } catch {
        case e: SQLServerException => println(f"Something Weird Happened: ${e.getMessage}")
      }
    }

    val results = monthly.coalesce(1)
    if (conf.getBoolean("toFile"))
      results.saveAsTextFile(conf.getString("file.path"))
    else
      results.collect().foreach(r => loadSQL(r, ""))

  }//END OF MAIN
}//END OF CLASS
