import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.json4s._
import org.json4s.jackson.JsonMethods._

object QueryFrequencies {

  case class Record(params: Params)
  case class Params(prd: Option[String], q: Option[String])

  def main (args: Array[String]): Unit = {

    val product = "14"

    val inputFilePath = "/user/root/orwell/data/i/hourly/*/*/*/*/*"
//    val inputFilePath = "/user/root/orwell/data/i/hourly/2015/01/30/09/i.2.0.509659.43417707.1422637200000.deflate"
    val outputFilePath = s"/results/Queries-$product"

    val sc = new SparkContext(new SparkConf().setAppName("QueryStrings"))
    val data = sc.textFile(inputFilePath)

    data.map(r => {implicit val format = DefaultFormats; parse(r).extract[Record]})
        .filter(r => r.params.prd.getOrElse("") == product)
        .map(r => (r.params.q.getOrElse(""), 1))
        .reduceByKey(_+_)
        .saveAsTextFile(outputFilePath)
  }

}
