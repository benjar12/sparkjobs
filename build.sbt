import AssemblyKeys._

name := "spark-jobs"

version := "2.2"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.2.1" % "provided",
  "org.apache.spark" % "spark-streaming_2.10" % "1.2.1" % "provided",
  "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.2.1",
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "joda-time" % "joda-time" % "2.7",
  "org.joda" % "joda-convert" % "1.7",
  "com.microsoft.sqlserver" % "sqljdbc4" % "4.0",
  "org.scalikejdbc" %% "scalikejdbc" % "2.2.5",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "com.typesafe" % "config" % "1.2.1",
  "org.mongodb" %% "casbah" % "2.8.1",
  "org.mongodb" %% "casbah-commons" % "2.8.1",
  "org.apache.commons" % "commons-email" % "1.3.3",
  "com.norbitltd" % "spoiwo" % "1.0.6",
  "com.novus" %% "salat" % "1.9.8"
)

assemblySettings

artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)

publishTo := ({
  val nexus = "http://ldevbd-e311.eliberation.com:8081/"
  if (isSnapshot.value)
    Some("Artifactory Realm" at nexus + "artifactory/libs-snapshot-local")
  else
    Some("Artifactory Realm"  at nexus + "artifactory/libs-release-local")
})

credentials += Credentials("Artifactory Realm", "ldevbd-e311.eliberation.com", "admin", "password")
